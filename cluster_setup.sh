#!bin/bash
aws sts get-caller-identity
eksctl create cluster --name k8s-cluster --node-type t2.micro --nodes 2 --nodes-min 2 --nodes-max 2 --region us-east-1
#update config
aws eks update-kubeconfig --region us-east-1 --name k8s-cluster